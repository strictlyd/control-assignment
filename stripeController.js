var config = require('./app.config');
var stripe = require('stripe')(config.secretKey);
var moment = require('moment');
var analysisController = require('./analysisController');

exports = module.exports = {};

module.exports = {

	listCustomers: function (customerLimit, callback) {
		stripe.customers.list({ 
	  			limit: customerLimit || 100
	  		},
		  	function (err, customers) {
		    	if (err) {
		    		console.log(err);
		    		return;
		    	}

		    	var customersLength = customers.data.length;
		    	var expiringlist = [];
		    	for (var i = 0; i < customersLength; i++) {
		    		module.exports.getCustomerCreditCards(customers.data[i].id, function (cards) {
						var cardsLength = cards.data.length;		
						for (var i = 0; i < cardsLength; i++) {
							// Check if each credit card is expiring
							// Return true if one is expiring
							if (analysisController.checkCreditCardExpiry(cards.data[i])) {
								console.log('Expiring ' + cards.data[i].customer + ':' + cards.data[i].id);
								var obj = {};
								obj[cards.data[i].customer] = cards.data[i].id
								expiringlist.push[obj];
							}
							console.log('Not expiring ' + cards.data[i].customer + ':' + cards.data[i].id);
						}		
		    		});
		    	}
		    	var obj = {};
		    	obj.customersWithExpiringCreditCards = expiringlist;
		    	callback(obj);
			}
		);
	},

	getCustomerCreditCards: function (customerId, callback) {
		stripe.customers.listCards(customerId, function (err, cards) {
			if (err) {
				console.log(err);
				return;
			}
			if (cards.has_more) {
				console.log('There are more results, please add pagination.');
			}
			callback(cards);
		});
	},

	calculateRevenue: function (callback) {
		// Get all the charges in the last 7 days and calculate the amount of each
		// TODO: Add functionality for pagination by recursion
		var sevenDaysAgo = moment().subtract(7, 'days').format('X');
		stripe.charges.list({
			limit: 100,
			created: {
				gte: sevenDaysAgo
			}},
			function (err, charges) {
				if (err) {
					console.log(err);
					return;
				}
				if (charges.has_more) {
					console.log('There are more results, please add pagination.');
				}
				analysisController.calculateRevenue(charges, function (revenue) {
					var obj = {};
					obj.revenueFromLastSevenDays = revenue;
					callback(obj);
				});
			}
		);
	},
	
	declinedCharges: function (chargesLimit, callback) {
		// If unspecified, get the last 100 charges in the last thirty days
		// chargesLimit specifies how many charges to return in the response 
		// TODO: Add functionality for pagination by recursion
		var thirtyDaysAgo = moment().subtract(30, 'days').format('X');
			
		stripe.charges.list({
			limit: chargesLimit || 100,
			created: {
				gte: thirtyDaysAgo
			}},
			function (err, charges) {
				if (err) {
					console.log(err);
					return;
				}
				if (charges.has_more) {
					console.log('There are more results, please add pagination.');
				}
				analysisController.calculateDeclinedCharges(charges, function (declinedCharges) {
					var obj = {};
					obj.declinedChargesFromLastThirtyDays = declinedCharges;
					callback(obj);
				});
			}
		);
	}
};