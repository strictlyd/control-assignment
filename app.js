var express = require('express');
// var config = require('./app.config');
// var stripe = require('stripe');
var stripeController = require('./stripeController');
var async = require('async');
var merge = require('merge');
var app = express();

var server = app.listen(3000, function () {
  var host = server.address().address;
  var port = server.address().port;
});

app.get('/', function (req, res) {

	// Calls asynchronously for every function in the array
	async.parallel([
	    function(callback) {
	        stripeController.listCustomers(4, function (customers) {
				callback(null, customers);
			});
	    },
	    function(callback){
	        stripeController.calculateRevenue(function (revenue) {
				callback(null, revenue);
			});
	    },
	    function(callback){
	    	stripeController.declinedCharges(null, function (charges) {
	    		callback(null, charges);
	    	})
	    }
	],
	// Callback when every function has succeeded or one has failed
	function(err, results){
		// Merge each result together into one response object
		var response = {};
		var length = results.length;
		for (var i = 0; i < length; i++) {
			response = merge(response, results[i]);
		}
		res.send(write(response));
	});
});

var write = function (res) {
	return JSON.stringify(res, null, 4);
}