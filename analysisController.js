exports = module.exports = {};
var moment = require('moment');

module.exports = {

	// generate

	// Synchronous check
	checkCreditCardExpiry: function (card) {
		// Assuming that the credit card expiry is set to 1 month
		// Return true if the credit card is about to expire next month
		var nextMonth = moment(new Date()).add(1, 'month').format('YYYY-M');
		var thisMonth = moment(new Date()).format('YYYY-M');
		var cardExpiry = moment(new Date(card.exp_year, card.exp_month)).format('YYYY-M');
		console.log(cardExpiry + ' ' + thisMonth + ' ' + nextMonth);
		if (moment(cardExpiry).isBetween(thisMonth, nextMonth)) {
			return true;
		}
		return false;
	},

	calculateRevenue: function (charges, callback) {
		var length = charges.data.length;
		var revenue = 0;
		for (var i = 0; i < length; i++) {
			if (charges.data[i].amount) {
				revenue += charges.data[i].amount;
				console.log(revenue);
			}
		}
		callback(revenue);
	},

	calculateDeclinedCharges: function (charges, callback) {
		// Iterate through the charges object and look for the failed charges 
		// And return results back to the callback
		var length = charges.data.length
		var fails = 0;
		var successes = 0;
		for (var i = 0; i < length; i++) {
			if (charges.data[i].status == 'failed') {
				fails++;
			}
		}
		callback(fails);
	}
}